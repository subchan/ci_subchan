<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class pembelian_model extends CI_Model
{
	//panggil nama table
	private $_table_header = "pembelian_header";
	private $_table_detail = "pembelian_detail";
	
	public function tampilDataPembelian()
	
	{
		$query = $this->db->query("SELECT * FROM " . $this->_table_header . " WHERE flag = 1");
		return $query->result();
	}
	
	public function savePembelianHeader()
	
	{
		$data['no_transaksi']	= $this->input->post('nomor_trans');
		$data['kode_supplier']	= $this->input->post('kode_supplier');
		$data['tanggal']	= date('Y-m-d');
		$data['approved']	= 1;
		$data['flag']	= 1;
		
		$this->db->insert($this->_table_header, $data);
	}
	
	public function savePembelianDetail()
	
	{
		$data['id_pembelian_d']	= $this->input->post('id_pembelian_d');
		$data['id_pembelian_h']	= $this->input->post('id_pembelian_h');
		$data['kode_barang']	= $this->input->post('kode_barang');
		$data['qty']	= $this->input->post('qty');
		$data['harga']	= $this->input->post('harga');
		$data['jumlah']	= $this->input->post('jumlah');
		$data['flag']	= 1;
		
		$this->db->insert($this->_table_detail, $data);
	}
	
	public function idTransaksiTerakhir()
	{
		$query = $this->db->query(
		"SELECT * FROM " . $this->_table_header . "WHERE flag = 1 ORDER BY
		id_pembelian_h DESC LIMIT 0,1"
		);
		$data_id = $query->result();
		
		foreach ($data_id as $data) {
			$last_id = $data->id_pembelian_h;
		}
		return $last_id;
	}
}
	