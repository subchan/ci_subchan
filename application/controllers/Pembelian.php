<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Pembelian extends CI_controller {

	public function __construct()
	
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("pembelian_model");
		$this->load->model("Supplier_model");
		$this->load->model("jenis_barang_model");
		
		//cek sesi login
		$user_login =$this->session->userdata();
		if (count($user_login)<=1){
			redirect("auth/index", "refresh");
		}
		
	}
	
	public function index()
	
	{
		$this->listSupplier();
	}
	
	public function listpembelian()
	
	{
		$data['data_supplier'] = $this->Supplier_model->tampilDataSupplier();
		$data['content']		= 'forms/list_pembelian';
		$this->load->view('home2', $data);
	}
	
	
		public function inputH()
	{
		$data['data_supplier'] = $this->Supplier_model->tampilDataSupplier();
		/*if (!empty($_REQUEST)) {
			$m_pembelian_h = $this->pembelian_model;
			$m_pembelian_h->savePembelianHeader();
			redirect("Pembelian/inputD" . $id_terakhir, "refresh");
			
			$id_terakhir = $pembelian_header->idTransaksiTerakhir(); 
		}*/
		$validation = $this->form_validation;
		$validation->set_rules($this->pembelian_model->rules());
		
		if ($validation->run()) {
			$this->pembelian_model->save();
			$this->session->set_flashdata('info', '<div style="color: green">Simpan Data Berhasil !</div>');
			redirect("Pembelian/index", "refresh");
		}
		
		$this->load->view('home2', $data);
	}
	
	public function inputD($id_pembelian_header)
	{
		$data['data_barang'] = $this->Barang_model->tampilDataBarang();
		$data['id_header'] = $id_pembelian_header;
		$data['data_pembelian_detail'] = $this->pembelian_model->tampilDataPembeianDetail($id_pembelian_header);
		/*if (!empty($_REQUEST)) {
			//save detail
			$this->pembelian_model->savePembelianDetail($id_pembelian_header);
			
			//proses update stock
			$kode_barang = $this->input->post('kode_barang');
			$qty = $this->input->post('qty');
			$this->barang_model->updateStock($kode_barang, $qty);
			
			redirect("Pembelian/inputdetail/" . $id_pembelian_header, "refresh");
		}*/
		$validation = $this->form_validation;
		$validation->set_rules($this->pembelian_model->rules());
		
		if ($validation->run()) {
			$this->pembelian_model->save();
			$this->session->set_flashdata('info', '<div style="color: green">Simpan Data Berhasil !</div>');
			redirect("Pembelian/index", "refresh");
		}
		
		$this->load->view('home2', $data);
	}
	
	  
}

